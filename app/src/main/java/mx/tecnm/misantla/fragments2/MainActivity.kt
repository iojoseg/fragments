package mx.tecnm.misantla.fragments2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity(), MiFragment.nombreListener {

    var nombreActual: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    nombreActual = findViewById(R.id.txtNombre)

    }

    override fun obtenerNombre(nombre: String) {
        super.obtenerNombre(nombre)
        nombreActual?.text = nombre
    }
}